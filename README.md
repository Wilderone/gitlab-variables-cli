### glvar - simple utility for managing gitlab CI variables
**Strongly recomended to use containerized version**

There are two ways to provide your credentials:
* With environment variables GITLAB_URL, GITLAB_PRIVATE_TOKEN
* With .ini file and GITLAB_CREDENTIALS variable with path to the file

Availabilities:
* Working with group
* Working with project
* Add and remove variables from JSON file
* Add and remove variables from command-line
* Change variables
* Mask variables
* List of variables



### Docker installation:

```
docker pull wilderone/glvar
```
Typical example
```
docker run -ti --rm -e GITLAB_CREDENTIALS=/app/creds.ini -e CLI_GITLAB_PROJECT=your_project -v /path/to/credentials.ini:/app/creds.ini -v /path/to/variables.json:/app/vars.json wilderone/glvar:latest -f /app/vars.json
```

*optional* set variables to control behavior of file mounts

*optional* use alias to simplify usage

If you define both environment variables and file with credentials/name, environment variables has higher priority

```
MOUNT_CREDS="-v ~/credentials.ini:/app/credentials.ini"
MOUNT_VARS="-v ~/variables.json:/app/variables.json"
PROJECT="-e CLI_GITLAB_GROUP=your_group"
DEFINE_CREDENTIALS="-e GITLAB_CREDENTIALS=/app/credentials.ini"

alias glvar='docker run -ti --rm ${MOUNT_CREDS} ${MOUNT_VARS} ${PROJECT} ${DEFINE_CREDENTIALS} wilderone/glvar'

glvar -l
glvar -p PROJECT_NAME -a KEY=VALUE
glvar -f /app/variables.json
```

**Important!**
.ini file should have this format:

```
[GITLAB]
GITLAB_URL=https://example.gitlab.com
GITLAB_PRIVATE_TOKEN=YOUR_TOKEN
```

--help:

```
This util was created to help you manage CI variables in Gitlab
    Use environment variables GITLAB_URL and GITLAB_PRIVATE_TOKEN for authorization.
    Also you can provide .ini with [GITLAB] header, example:
    > cat /app/credentials.ini
    [GITLAB]
    GITLAB_URL=https://example.gitlab.com
    GITLAB_PRIVATE_TOKEN=YOUR_TOKEN

    Use GITLAB_CREDENTIALS variable to define path

    Project or group provided via cli (-p / -g) has the highest priority than ENV variables
    -p  -  set project name (-p MyProject) or use CLI_GITLAB_PROJECT environment variable
    -g  -  set group name (-g MyGroup) or use CLI_GITLAB_GROUP environment variable
    -l  -  list of variables
    -la -  list of variables in every project of group (use with -g GROUP_NAME) 
    -a  -  add variable (-a KEY=VALUE), use -f for miltiple variables
    -f  -  add variables from JSON file (-f /path/to/file)
    -m  -  mask added variable(s). Be sure variable meet regular expression requirements.
    -rm -  remove variable (-rm KEY), use list to delete multiple vars (-rm KEY1,KEY2,KEY3)
    -rmf - remove variables, passed from JSON ( {"KEY":"VALUE"} )file (-rmf /path/to/file) 
    -cv -  change value of variable (-cv NAME=NEW_VALUE)
```


